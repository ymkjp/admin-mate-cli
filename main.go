package main

import (
	"bitbucket.org/i05/admin-mate-cli/am"
	"github.com/urfave/cli"
	"log"
	"os"
)

var Version string = "0.0.1"

func main() {
	err := CreateApp().Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}

func CreateApp() *cli.App {
	app := cli.NewApp()
	app.Usage = "A command-line tool for Atlassian Cloud administrators"
	app.Description = `Note that these commands only can retrieve information accessible to the specified account.
So make sure to grant necessary permissions to the account.
`
	app.Version = Version
	app.Author = "Kenta Yamamoto"
	app.Email = "kyamamoto@atlassian.com"
	app.Commands = am.Commands
	return app
}
