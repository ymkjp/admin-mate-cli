#!/usr/bin/env bash
# https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html
# https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/downloads
set -eu

DOCUMENT_URL="https://confluence.atlassian.com/bitbucket/deploy-build-artifacts-to-bitbucket-downloads-872124574.html"
API_ENDPOINT="https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER:-i05}/${BITBUCKET_REPO_SLUG:-admin-mate-cli}/downloads"
FILE_SUFFIX="${VERSION:=v0.1.x}"

if [[ -z "${BB_AUTH_STRING:-}" ]]; then
  echo "Specify \`BB_AUTH_STRING=<username>:<api_token>\`. Refer to ${DOCUMENT_URL} fore more details."
  exit 1
fi

cd ./pkg
for FILE_PATH in $(find . -type f ! -name '.*'); do
  BASENAME="$(basename "$(dirname "${FILE_PATH}")")"
  ARTIFACT="./${BASENAME}_${FILE_SUFFIX//\./-}.zip"
  zip -r "${ARTIFACT}" "${FILE_PATH}"
  curl -LX POST --user "${BB_AUTH_STRING}" "${API_ENDPOINT}" --form files=@"${ARTIFACT}"
done
