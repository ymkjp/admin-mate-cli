#!/usr/bin/env bash
# https://bitbucket.org/atlassian/pipelines-examples-go
# https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html

ci () {
  set -e
  PACKAGE_PATH="${GOPATH}/src/bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}"
  VERSION="${BITBUCKET_TAG:-$(echo "$BITBUCKET_COMMIT" | cut -c1-7)-custom}"
  CLONE_DIR="${BITBUCKET_CLONE_DIR}"
  CURRENT_DIR="$(pwd)"

  setup () {
    export VERSION CLONE_DIR
    mkdir -pv "${PACKAGE_PATH}"
    tar -cO . | tar -xv -C "${PACKAGE_PATH}"
  }

  init () {
    echo "HOME:${HOME},CURRENT_DIR:${CURRENT_DIR},PACKAGE_PATH:${PACKAGE_PATH}"
    setup
    cd "${PACKAGE_PATH}"
    make init
  }

  release () {
    setup
    cd "${PACKAGE_PATH}"
    make init release
  }

  fatal () {
    MESSAGE="${1:-Something went wrong.}"
    echo "[$(basename "$0")] ERROR: ${MESSAGE}" >&2
    exit 1
  }

  usage () {
    SELF="$(basename "$0")"
    echo -e "${SELF}
    \\nUsage: ${SELF} [arguments]
    \\nArguments:"
    declare -F | awk '{print "\t" $3}' | grep -v "${SELF}"
  }

  if [[ $# = 0 ]]; then
    usage
  elif [[ "$(type -t "$1")" = "function" ]]; then
    $1 "$(shift && echo "$@")"
  else
    fatal "No such command: $*"
  fi
}

ci "$@"
