# https://www.gnu.org/software/make/manual/html_node/Automatic-Variables.html

CLONE_DIR ?= $$(pwd)
VERSION ?= $$(git describe --tags --always --dirty) ($$(git name-rev --name-only HEAD))
BUILD_FLAGS := -ldflags "\
	      -X \"main.Version=$(VERSION)\" \
	      "
TARGET_LINTERS := lint-shell lint-yaml lint-go
SHELL_FILES := $(wildcard ./script/*.sh)
SHELL_REPORT := ./report/lint-shell.txt
YAML_FILES := $(wildcard ./*.yml)
YAML_REPORT := ./report/lint-yaml.txt
GO_FILES := $(shell find . -type f -name "*.go")
GO_TESTED := ./report/test-go.txt

.PHONY: init dependencies install build test lint $(TARGET_LINTERS) release clean

init: dependencies install lint build test

dependencies:
	go version
	docker --version

install:
	go get -t -v

build:
	go build -v $(BUILD_FLAGS)

test: $(GO_TESTED)

lint: $(TARGET_LINTERS)
lint-shell: $(SHELL_REPORT)
lint-yaml: $(GO_LINT)
lint-go:
	gofmt -s -w .
	go vet .

release:
	go get -v github.com/mitchellh/gox
	VERSION="$(VERSION)" ./script/release.sh
	VERSION="$(VERSION)" ./script/distribution.sh
	touch pkg/.gitkeep

clean:
	docker system prune --volumes

$(SHELL_REPORT): $(SHELL_FILES)
	docker run --rm -v "${CLONE_DIR}:/mnt" koalaman/shellcheck:v0.6.0 \
    		--exclude=SC1090,SC2044 \
    		$?
	touch $(SHELL_REPORT)

$(YAML_REPORT): $(YAML_FILES)
	docker run --rm -v "${CLONE_DIR}:/workdir" giantswarm/yamllint $?
	touch $(YAML_REPORT)

$(GO_TESTED): $(GO_FILES)
	CLONE_DIR="$(CLONE_DIR)" go test -v ./...
	touch $(GO_TESTED)
