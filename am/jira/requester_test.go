package jira

import (
	"bitbucket.org/i05/admin-mate-cli/am/helper"
	"flag"
	"fmt"
	"github.com/ToQoz/gopwt"
	"github.com/ToQoz/gopwt/assert"
	"github.com/andygrunwald/go-jira"
	"github.com/google/go-querystring/query"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	flag.Parse()
	gopwt.Empower()
	os.Exit(m.Run())
}

type Mock struct {
	credentials *Credentials
	requester   *Requester
	Mux         *http.ServeMux
	Server      *httptest.Server
}

var mock = &Mock{
	credentials: &Credentials{
		SiteUrl: "https://example.atlassian.net ",
		Username: "admin@example.com	",
		Token: `
foo
	`,
	},
}

func (m *Mock) EncodeQuery(t *testing.T, q SearchQuery) string {
	t.Helper()
	v, err := query.Values(q)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	return v.Encode()
}

func (m *Mock) teardown() {
	mock.Server.Close()
}

/**
https://github.com/andygrunwald/go-jira/blob/e2a9240a5c/issue_test.go#L522-L550
*/
func (m *Mock) setup() {
	mock.requester = &Requester{
		credentials: mock.credentials,
	}
	mock.credentials.init()
	err := mock.requester.init()
	if err != nil {
		panic(err)
	}
	mock.Mux = http.NewServeMux()
	mock.Server = httptest.NewServer(mock.Mux)
	client, err := jira.NewClient(nil, mock.Server.URL)
	if err != nil {
		panic(err)
	}
	mock.requester.client = client
	mock.requester.Interval = 0
}

func TestCredentials_Init(t *testing.T) {
	mock.setup()
	defer mock.teardown()
	assert.OK(t, mock.credentials.SiteUrl == "https://example.atlassian.net")
	assert.OK(t, mock.credentials.Username == "admin@example.com")
	assert.OK(t, mock.credentials.Token == "foo")
}

func TestRequester_Init(t *testing.T) {
	mock.setup()
	defer mock.teardown()
	t.Run("it has required params", func(t *testing.T) {
		r := Requester{}
		err := r.init()
		assert.OK(t, err != nil)
	})
	t.Run("it has properties", func(t *testing.T) {
		assert.OK(t, mock.requester.log != nil)
		assert.OK(t, mock.requester.client != nil)
	})
}

func TestRequester_AttachmentMeta(t *testing.T) {
	mock.setup()
	defer mock.teardown()
	mock.Mux.HandleFunc(RestApiV3AttachmentMeta, func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, err := fmt.Fprint(
			w,
			helper.Response(t, "jira-attachment-meta.json"))
		if err != nil {
			t.Error(err)
		}
	})
	meta, err := mock.requester.attachmentMeta(&AttachmentMeta{})
	assert.OK(t, err == nil)
	assert.OK(t, meta != nil)
	assert.OK(t, meta.Enabled)
	assert.OK(t, meta.UploadLimit > 0)
}

func TestRequester_FindAll(t *testing.T) {
	mock.setup()
	defer mock.teardown()
	q1 := SearchQuery{
		Jql:        "attachments IS NOT EMPTY",
		StartAt:    0,
		MaxResults: 2,
		Fields:     "attachment",
	}
	q2 := q1
	q2.StartAt += q2.MaxResults
	mock.Mux.HandleFunc(RestApiV3Search, func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		var s string
		switch r.URL.RawQuery {
		case mock.EncodeQuery(t, q1):
			s = helper.Response(t, "jira-search-1.json")
		case mock.EncodeQuery(t, q2):
			s = helper.Response(t, "jira-search-2.json")
		default:
			t.Errorf("Query didn't match: %v", r.URL.String())
			t.FailNow()
		}
		_, err := fmt.Fprint(w, s)
		if err != nil {
			t.Error(err)
		}
	})
	results, err := mock.requester.findAll([]*SearchResult{}, q1)
	assert.OK(t, err == nil)
	assert.OK(t, len(results) > 1)
}
