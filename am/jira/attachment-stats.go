package jira

import (
	"bitbucket.org/i05/admin-mate-cli/am/util"
	"encoding/json"
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/urfave/cli"
	"log"
)

type AttachmentStats struct {
	log                              *log.Logger
	r                                *Requester
	Credentials                      *Credentials
	AttachmentMeta                   *AttachmentMeta
	SearchResults                    []*SearchResult
	TotalAttachmentSizeByte          uint64
	TotalAttachmentSizeHumanReadable string
	TotalAttachmentCount             uint64
}

func PrintAttachmentStats(c *cli.Context) error {
	data := &AttachmentStats{r: &Requester{credentials: &Credentials{
		SiteUrl:  c.String("site-url"),
		Username: c.String("admin-email"),
		Token:    c.String("api-token"),
	}},
	}
	err := data.perform()
	if err != nil {
		return err
	}
	result, err := json.Marshal(data)
	if err != nil {
		return err
	}
	_, err = fmt.Fprintln(c.App.Writer, string(result))
	return err
}

func (a *AttachmentStats) init() error {
	a.log = util.CreateLogger(a)
	err := a.r.init()
	if err != nil {
		return err
	}
	return nil
}

func (a *AttachmentStats) perform() error {
	err := a.init()
	if err != nil {
		return err
	}
	meta, err := a.r.attachmentMeta(&AttachmentMeta{})
	if err != nil {
		return err
	}
	results, err := a.r.findAll(
		[]*SearchResult{},
		SearchQuery{
			Jql:        "attachments IS NOT EMPTY",
			StartAt:    0,
			MaxResults: 100,
			Fields:     "attachment",
		})
	if err != nil {
		return err
	}
	a.Credentials = &Credentials{
		SiteUrl:  a.r.credentials.SiteUrl,
		Username: a.r.credentials.Username,
	}
	a.AttachmentMeta = meta
	a.SearchResults = results
	a.sum()

	return nil
}

func (a *AttachmentStats) sum() {
	for _, sr := range a.SearchResults {
		for _, issue := range sr.Issues {
			for _, attachment := range issue.Fields.Attachments {
				a.TotalAttachmentCount += 1
				a.TotalAttachmentSizeByte += uint64(attachment.Size)
			}
		}
	}
	a.TotalAttachmentSizeHumanReadable = humanize.IBytes(a.TotalAttachmentSizeByte)
}
