package jira

/**
https://developer.atlassian.com/cloud/jira/platform/rest/v3/
*/

import (
	"bitbucket.org/i05/admin-mate-cli/am/util"
	"errors"
	"github.com/andygrunwald/go-jira"
	"github.com/google/go-querystring/query"
	"log"
	"strings"
	"time"
)

const DefaultInterval = 10 * time.Millisecond

type Credentials struct {
	SiteUrl  string
	Username string
	Token    string
}

func (c *Credentials) init() {
	c.SiteUrl = strings.TrimSpace(c.SiteUrl)
	c.Username = strings.TrimSpace(c.Username)
	c.Token = strings.TrimSpace(c.Token)
}

type Requester struct {
	log         *log.Logger
	client      *jira.Client
	credentials *Credentials
	Interval    time.Duration
}

func (r *Requester) init() error {
	r.log = util.CreateLogger(r)
	if r.credentials == nil {
		return errors.New("credentials are required to initialize requester")
	}
	r.credentials.init()
	tp := jira.BasicAuthTransport{
		Username: r.credentials.Username,
		Password: r.credentials.Token,
	}
	client, err := jira.NewClient(tp.Client(), r.credentials.SiteUrl)
	if err != nil {
		return err
	}
	r.client = client
	r.Interval = DefaultInterval
	return nil
}

// JQL
// https://confluence.atlassian.com/jirasoftwarecloud/advanced-searching-fields-reference-764478339.html
type SearchResult struct {
	StartAt    int
	MaxResults int
	Total      int
	Issues     []jira.Issue
}

type SearchQuery struct {
	Jql        string `url:"jql,omitempty"`
	StartAt    int    `url:"startAt,omitempty"`
	MaxResults int    `url:"maxResults,omitempty"`
	Fields     string `url:"fields,omitempty"`
}

// https://developer.atlassian.com/cloud/jira/platform/rest/v3/#api-api-3-search-get
const RestApiV3Search = "/rest/api/3/search"

func (r *Requester) findAll(d []*SearchResult, q SearchQuery) ([]*SearchResult, error) {
	req, _ := r.client.NewRawRequest("GET", RestApiV3Search, nil)
	v, err := query.Values(q)
	if err != nil {
		return nil, err
	}
	req.URL.RawQuery = v.Encode()
	r.log.Println("findAll.URL:", req.URL)
	result := &SearchResult{}
	_, err = r.client.Do(req, result)
	if err != nil {
		return nil, err
	}
	d = append(d, result)
	i, max, total := q.StartAt, q.MaxResults, result.Total
	nextStart := (i + 1) * max
	r.log.Printf("findAll.foundIssues: (StartAt,MaxResults,Total,nextStart)=(%v,%v,%v, %v)",
		i, max, total, nextStart)
	if total > nextStart {
		time.Sleep(r.Interval)
		q.StartAt = nextStart
		return r.findAll(d, q)
	}
	return d, nil
}

//func (r *Requester) field() error {
//	req, _ := r.client.NewRequest("GET", "rest/api/3/field", nil)
//	res, err := r.client.Do(req, nil)
//	dump, _ := httputil.DumpResponse(res.Response, true)
//	fmt.Println("field:", string(dump))
//	return err
//}

type AttachmentMeta struct {
	Enabled     bool
	UploadLimit int64
}

const RestApiV3AttachmentMeta = "/rest/api/3/attachment/meta"

func (r *Requester) attachmentMeta(d *AttachmentMeta) (*AttachmentMeta, error) {
	req, _ := r.client.NewRequest("GET", RestApiV3AttachmentMeta, nil)
	_, err := r.client.Do(req, d)
	if err != nil {
		return nil, err
	}
	return d, nil
}
