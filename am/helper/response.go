package helper

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

/**
Fixture
*/
func Response(t *testing.T, path string) string {
	t.Helper()
	dir := os.Getenv("CLONE_DIR")
	if dir == "" {
		panic("environmental variable `CLONE_DIR` must be defined")
	}
	data, err := ioutil.ReadFile(filepath.Join(
		dir,
		"testdata",
		path))
	if err != nil {
		t.Error(err)
	}
	return string(data)
}
