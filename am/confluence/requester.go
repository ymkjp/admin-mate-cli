package confluence

import (
	"bitbucket.org/i05/admin-mate-cli/am/util"
	"github.com/essentialkaos/go-confluence"
	"log"
	"strings"
	"time"
)

const DefaultInterval = 10 * time.Millisecond

type Credentials struct {
	SiteUrl  string
	Username string
	Token    string
}

func (cr *Credentials) init() {
	cr.SiteUrl = strings.TrimSpace(cr.SiteUrl) + "/wiki"
	cr.Username = strings.TrimSpace(cr.Username)
	cr.Token = strings.TrimSpace(cr.Token)
}

type Requester struct {
	log         *log.Logger
	client      *confluence.API
	credentials *Credentials
	Interval    time.Duration
}

func (r *Requester) init() error {
	r.log = util.CreateLogger(r)
	r.credentials.init()
	client, err := confluence.NewAPI(
		r.credentials.SiteUrl,
		r.credentials.Username,
		r.credentials.Token)
	if err != nil {
		return err
	}
	r.client = client
	r.Interval = DefaultInterval
	return nil
}

type SearchResults = []*confluence.ContentCollection

/**
https://developer.atlassian.com/cloud/confluence/rest/#api-space-spaceKey-content-get
*/
func (r *Requester) findAll(d SearchResults, q confluence.ContentParameters) (SearchResults, error) {
	r.log.Printf("findAll.query: %#v", q)
	result, err := r.client.GetContent(q)
	if err != nil {
		return nil, err
	}
	d = append(d, result)
	i, max, total := q.Start, q.Limit, result.Size
	nextStart := (i + 1) * max
	r.log.Printf("findAll.foundIssues: (StartAt,MaxResults,Total,nextStart)=(%v,%v,%v, %v)",
		i, max, total, nextStart)
	if total > nextStart {
		time.Sleep(r.Interval)
		q.Start = nextStart
		return r.findAll(d, q)
	}
	return d, nil
}
