package confluence

import (
	"encoding/json"
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/essentialkaos/go-confluence"
	"github.com/urfave/cli"
)

type AttachmentStats struct {
	r                                *Requester
	SearchResults                    SearchResults
	TotalAttachmentSizeByte          uint64
	TotalAttachmentSizeHumanReadable string
	TotalAttachmentCount             uint64
}

// https://developer.atlassian.com/cloud/confluence/rest/
func PrintAttachmentStats(c *cli.Context) error {
	data := &AttachmentStats{r: &Requester{credentials: &Credentials{
		SiteUrl:  c.String("site-url"),
		Username: c.String("admin-email"),
		Token:    c.String("api-token"),
	}}}
	err := data.perform()
	if err != nil {
		return err
	}
	result, err := json.Marshal(data)
	if err != nil {
		return err
	}
	fmt.Println(string(result))
	return nil
}

func (a *AttachmentStats) perform() error {
	err := a.r.init()
	if err != nil {
		return err
	}
	query := confluence.ContentParameters{
		Limit:  200,
		Start:  0,
		Type:   confluence.CONTENT_TYPE_PAGE,
		Status: "any",
		Expand: []string{"descendants.attachment"},
	}
	results, err := a.r.findAll(SearchResults{}, query)
	if err != nil {
		return err
	}
	query.Type = confluence.CONTENT_TYPE_BLOGPOST
	results, err = a.r.findAll(results, query)
	if err != nil {
		return err
	}

	a.SearchResults = results
	a.sum()

	return nil
}

func (a *AttachmentStats) sum() {
	for _, collection := range a.SearchResults {
		for _, content := range collection.Results {
			for _, attachment := range content.Descendants.Attachments.Results {
				a.TotalAttachmentCount += 1
				a.TotalAttachmentSizeByte += uint64(attachment.Extensions.FileSize)
			}
		}
	}
	a.TotalAttachmentSizeHumanReadable = humanize.IBytes(a.TotalAttachmentSizeByte)
}
