
[![master](https://img.shields.io/bitbucket/pipelines/i05/admin-mate-cli.svg)](https://bitbucket.org/i05/admin-mate-cli/addon/pipelines/home)

#### Usage

0. Download package at https://bitbucket.org/i05/admin-mate-cli/downloads/
0. Prepare required parameters
    * `AM_SITE_URL` https://my.atlassian.com/
    * `AM_ADMIN_EMAIL` https://id.atlassian.com/manage-profile/email
    * `AM_API_TOKEN` https://id.atlassian.com/manage/api-tokens
0. Execute commands like as following:

```bash
export AM_SITE_URL="https://example.atlassian.net" AM_ADMIN_EMAIL="admin@example.com" AM_API_TOKEN="__YOURS__"
./admin-mate-cli help
```

```text
NAME:
   admin-mate-cli - A command-line tool for Atlassian Cloud administrators

USAGE:
   admin-mate-cli [global options] command [command options] [arguments...]

VERSION:
   v0.2.0-2-g99fd806-dirty (master)

DESCRIPTION:
   
Note that these commands only can retrieve information accessible to the specified account.
So make sure to grant necessary permissions to the account.


AUTHOR:
   Kenta Yamamoto <kyamamoto@atlassian.com>

COMMANDS:
     jira-attachment-stats        Shows Jira's attachment statistics
     confluence-attachment-stats  Shows Confluence's attachment statistics
     help, h                      Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```


#### Contribution

Here's how to get started!

0. Install runtime environments below
    * Golang
    * Docker
0. Then, start developing with `make init`

Detailed dependencies are clarified at CI setting (refer to `bitbucket-pipelines.ym`).
